/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasp_controll;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author onelove
 */
public class Controler {

    private static GpioController gpio;
    private static Map< String, GpioPinDigitalOutput> pinList;

    public static Map< String, GpioPinDigitalOutput> getPinList() {
        return pinList;
    }
    private final static int hop = 2;

    private final synchronized static void registerPin(String name, GpioPinDigitalOutput pin) {
        pinList.put(name, pin);
    }

    public synchronized final static void startControler() {
        // create gpio controller
        gpio = GpioFactory.getInstance();
        pinList = new HashMap<>();
        System.out.println("Controler Initialized");
    }

    public synchronized final static void closeControler() {
        // stop all GPIO activity/threads by shutting down the GPIO controller
        // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
        gpio.shutdown();
        unregisterAllPins();
    }

    private synchronized final static void unregisterAllPins() {
        for (Map.Entry<String, GpioPinDigitalOutput> entry : pinList.entrySet()) {
//            String key = entry.getKey();
//            GpioPinDigitalOutput value = entry.getValue();
            gpio.unprovisionPin(entry.getValue());
        }
    }

    public synchronized final static void unregisterPin(String name) {
        gpio.unprovisionPin(pinList.get(name));
    }

    public synchronized final static void createDigitalOutPut_fromPin(Pin pin, String name, PinState state) {
        registerPin(name, gpio.provisionDigitalOutputPin(pin, name, state));
    }

    public synchronized static void shutDownPin(GpioPinDigitalOutput pin) throws InterruptedException {
        pin.setShutdownOptions(true, PinState.LOW);
        System.out.println(pin.getPin());
        System.out.println("--> GPIO pin :: " + pin.getName() + " state should be: OFF");
        // turn off gpio pin #01
        pin.low();
    }

    public synchronized static void enablePin(GpioPinDigitalOutput pin) throws InterruptedException {
        pin.setShutdownOptions(true, PinState.HIGH);
        System.out.println(pin.getPin());
        System.out.println("--> GPIO pin :: " + pin.getName() + " state should be: ON");
        // turn off gpio pin #01
        pin.toggle();
    }

    public static PinState checkPinState(GpioPinDigitalOutput pin) throws InterruptedException {
        return pin.getState();
    }

    public synchronized static void enablePin_ForSpacificTime(GpioPinDigitalOutput pin, int time) throws InterruptedException {
        System.out.println("--> GPIO pin :: " + pin.getName() + "state should be: ON for only 1 second");
        pin.pulse(time * 1000, true); // set second argument to 'true' use a blocking call
    }

    /*public synchronized static void start_blinking() throws InterruptedException {

        System.out.println("<--Pi4J--> GPIO Control Example ... started.");

        startControler();
        createDigitalOutPut_fromPin(RaspiPin.GPIO_01, "Exaust", PinState.HIGH);
        createDigitalOutPut_fromPin(RaspiPin.GPIO_04, "radiator", PinState.HIGH);

        // set shutdown state for this pin
        shutDownPin(pinList.get("Exaust"));
        shutDownPin(pinList.get("radiator"));
        System.out.println("Pins sould be off...");
        Thread.sleep(hop * 1000);

        enablePin(pinList.get("Exaust"));
        enablePin(pinList.get("radiator"));
        System.out.println("Pins sould be on...");
        Thread.sleep(hop * 1000);

        shutDownPin(pinList.get("Exaust"));
        shutDownPin(pinList.get("radiator"));
        System.out.println("Pins sould be off...");
        Thread.sleep(hop * 1000);

//        blinking purposes
        enablePin(pinList.get("Exaust"));
        enablePin(pinList.get("radiator"));
        System.out.println("Pins sould be on...");
        Thread.sleep(hop * 1000);

        shutDownPin(pinList.get("Exaust"));
        shutDownPin(pinList.get("radiator"));
        System.out.println("Pins sould be off...");
        Thread.sleep(hop * 1000);

        enablePin_ForSpacificTime(pinList.get("Exaust"), 15);
        // turn on gpio pin #01 for 1 second and then off

        closeControler();

        System.out.println("Exiting ControlGpioExample");
    }*/
//END SNIPPET: control-gpio-snippet
}
