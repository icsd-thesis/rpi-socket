/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasp_controll;

import com.google.gson.Gson;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static rasp_controll.Controler.checkPinState;
import static rasp_controll.Controler.createDigitalOutPut_fromPin;
import static rasp_controll.Controler.enablePin;
import static rasp_controll.Controler.enablePin_ForSpacificTime;
import static rasp_controll.Controler.shutDownPin;
import shared.Region;

/**
 *
 * @author onelove
 */
public class Radiator_controler {

    private final static Gson jsonfy = new Gson();
    private final static Pin[] avalable_pins = {RaspiPin.GPIO_25, RaspiPin.GPIO_02, RaspiPin.GPIO_03};

    private static Map<Region, Pin> set;

    static {
        set = new HashMap<Region, Pin>();
        set.put(Region.KITCHEN, avalable_pins[0]);
        set.put(Region.BEDROOM, avalable_pins[1]);
        set.put(Region.LIVING_ROOM, avalable_pins[2]);
    }

    public final MyPinState getPinState() throws InterruptedException {
        return new MyPinState(Controler.checkPinState(Controler.getPinList().get(name)), name);
    }

    @Override
    public String toString() {
        return jsonfy.toJson(this);
    }

    private static final Pin givePinBack(Pin pin) {
        Pin temp = null;
        for (int i = 0; i < avalable_pins.length; i++) {
            if (avalable_pins[i] == null) {
                avalable_pins[i] = pin;
            } else {
                continue;
            }

        }
        return temp;
    }

    private boolean state;
    private Region region;
    private String name;
    private Pin cuuPin;

    public final String getName() {
        return this.name;
    }
//    public Radiator_controler(boolean state, Region region, String name) {
//        this.state = state;
//        this.region = region;
//        this.name = name;
//        this.cuuPin = getNextAvailablePin();
//        createDigitalOutPut_fromPin(getCuuPin(), name, PinState.LOW);
//    }
//
//    public Radiator_controler(Pin pin, Region region, String name) {
//        this.state = state;
//        this.region = region;
//        this.name = name;
//        this.cuuPin = pin;
//        createDigitalOutPut_fromPin(getCuuPin(), name, PinState.LOW);
//    }

    public Radiator_controler(Region region, String name) {
        this.state = state;
        this.region = region;
        this.name = name;
        this.cuuPin = set.get(region);
        System.out.println("Region : " + region + " pin name : " + cuuPin.getName());
        createDigitalOutPut_fromPin(getCuuPin(), name, PinState.LOW);
    }

    public Pin getCuuPin() {
        return cuuPin;
    }

    public void start_radiator() throws InterruptedException {
        if (!state) {
            enablePin(Controler.getPinList().get(name));
            System.out.println("Radiator controler :: " + Controler.getPinList().get(name));
            this.state = true;
        } else {
            System.err.println("The module is already up...");
        }
    }

    public void stop_radiator() throws InterruptedException {
        if (state) {
            shutDownPin(Controler.getPinList().get(name));
            System.out.println(Controler.getPinList().get(name));
            this.state = false;
        } else {
            System.err.println("The module is already dwun...");
        }
    }

    public final void deleteRadiator() {
        Controler.unregisterPin(getCuuPin().getName());
        givePinBack(getCuuPin());

    }

    public void start_for_spacific_time_radiator(int time) throws InterruptedException {
        if (!state) {
            enablePin_ForSpacificTime(Controler.getPinList().get(name), time);
        } else {
            System.err.println("The module is already up...");
        }
    }
}
