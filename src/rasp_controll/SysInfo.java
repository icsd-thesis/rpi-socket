/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rasp_controll;

import com.google.gson.Gson;
import java.util.List;
import com.pi4j.platform.PlatformManager;
import com.pi4j.system.NetworkInfo;
import com.pi4j.system.SystemInfo;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 *
 * @author onelove
 */
public class SysInfo {

    private final static Gson json = new Gson ();
    //    platform details
    private String platform_name;
    private String platform_id;

    //    cpu details
    private String cpu_revision;
    private String cpu_architecture;
    private String cpu_part;
    private float cpu_temprature;
    private float cpu_voltage;
    private String cpu_model_name;
    private String cpu_processor;

    //    hardware details
    private String serial_num;
    private String harware;
    private String harware_revision;
    private String board_type;

    //    memory details
    private long total_memory;
    private long used_memory;
    private long free_memory;
    private long shared_memory;
    private long memory_buffers;
    private long cached_memory;
    private float sdram_c_voltage;
    private float sdram_i_voltage;
    private float sdram_p_voltage;

    //    Os details
    private String os_name;
    private String os_version;
    private String os_architecture;
    private String os_firmware_build;
    private String os_firmware_date;

    //    NetworkDetails
    private String hostname;
    private List<String> ip_add;
    private List<String> fqdn;
    private List<String> nameServers;

    public SysInfo(boolean platform, boolean cpu, boolean hardware, boolean memory, boolean os, boolean network) throws IOException, InterruptedException, ParseException {
        if (platform) {
            this.platform_name = PlatformManager.getPlatform().getLabel();
            this.platform_id = PlatformManager.getPlatform().getId();
        }
        if (cpu) {
            this.cpu_revision = SystemInfo.getCpuRevision();
            this.cpu_architecture = SystemInfo.getCpuArchitecture();
            this.cpu_part = SystemInfo.getCpuPart();
            this.cpu_temprature = SystemInfo.getCpuTemperature();
            this.cpu_voltage = SystemInfo.getCpuVoltage();
            this.cpu_model_name = SystemInfo.getModelName();
            this.cpu_processor = SystemInfo.getProcessor();
        }
        if (hardware) {
            this.serial_num = SystemInfo.getSerial();
            this.harware = SystemInfo.getHardware();
            this.harware_revision = SystemInfo.getRevision();
            this.board_type = SystemInfo.getBoardType().name();
        }
        if (memory) {
            this.total_memory = SystemInfo.getMemoryTotal();
            this.used_memory = SystemInfo.getMemoryUsed();
            this.free_memory = SystemInfo.getMemoryFree();
            this.shared_memory = SystemInfo.getMemoryShared();
            this.memory_buffers = SystemInfo.getMemoryBuffers();
            this.cached_memory = SystemInfo.getMemoryCached();
            this.sdram_c_voltage = SystemInfo.getMemoryVoltageSDRam_C();
            this.sdram_i_voltage = SystemInfo.getMemoryVoltageSDRam_I();
            this.sdram_p_voltage = SystemInfo.getMemoryVoltageSDRam_P();
        }
        if (os) {
            this.os_name = SystemInfo.getOsName();
            this.os_version = SystemInfo.getOsVersion();
            this.os_architecture = SystemInfo.getOsArch();
            this.os_firmware_build = SystemInfo.getOsFirmwareBuild();
            this.os_firmware_date = SystemInfo.getOsFirmwareDate();
        }
        if (network) {
            ip_add = new ArrayList<String>();
            fqdn = new ArrayList<String>();
            nameServers = new ArrayList<String>();
            this.hostname = NetworkInfo.getHostname();
            for (String ipAddress : NetworkInfo.getIPAddresses()) {
                ip_add.add(ipAddress);
            }
            for (String fqdn : NetworkInfo.getFQDNs()) {
                this.fqdn.add(fqdn);
            }
            for (String nameserver : NetworkInfo.getNameservers()) {
                this.nameServers.add(nameserver);
            }
        }
    }

    @Override
    public String toString() {
        return json.toJson(this);
    }
    
}
