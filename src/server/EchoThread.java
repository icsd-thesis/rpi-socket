package server;

import automation_controler.AutomationControler;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import rasp_controll.Exaust_controler;
import rasp_controll.LightsControler;
import rasp_controll.MyPinState;
import rasp_controll.Radiator_controler;
import rasp_controll.SysInfo;
import static server.Server.getAutomationControler;
import static server.Server.getExaust_controler;
import static server.Server.getLights_controler;
import static server.Server.getRadiator_controler;
import static server.Server.startAutomation;
import static server.Server.stoptAutomation;
import static shared.Actions.*;
import shared.Auto_Controler_config;
import shared.Message;
import shared.Region;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author onelove
 */
public class EchoThread extends Thread {

    protected Socket socket;
    protected static ObjectOutputStream writeFile;

    public EchoThread(Socket clientSocket) {
        this.socket = clientSocket;
        System.out.println("User :: " + this.socket.toString());
    }

    public void run() {
        try {
            ObjectInputStream inp = null;

            ObjectOutputStream out = null;
            Message message = null;

            try {
                inp = new ObjectInputStream(socket.getInputStream());
                out = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            message = (Message) inp.readObject();
            Message msg = findActionBasedonMessage(message);
            if (msg != null) {
                out.writeObject(msg);
            }
            out.flush();
        } catch (InterruptedException ex) {
            Logger.getLogger(EchoThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EchoThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EchoThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Message generateRequestedInfo(Message msg) throws IOException, InterruptedException, ParseException {
        boolean memeory = false, platform = false, cpu = false, hardware = false, os = false, network = false;
        for (int i = 0; i < msg.getInfoList().size(); i++) {
            switch (msg.getInfoList().get(i)) {
                case PLATFORM: {
                    platform = true;
                    break;
                }
                case CPU: {
                    cpu = true;
                    break;
                }
                case HARWARE: {
                    hardware = true;
                    break;
                }
                case MEMORY: {
                    memeory = true;
                    break;
                }
                case OS: {
                    os = true;
                    break;
                }
                case NETWORK: {
                    network = true;
                    break;
                }
            }
        }
        SysInfo info = new SysInfo(platform, cpu, hardware, memeory, os, network);
        return new Message(info.toString());
    }

    private Message check_for_info_req(Message msg) throws IOException, InterruptedException {
        if (msg.getAction() == SYS_INFO) {
            try {
                return generateRequestedInfo(msg);
            } catch (ParseException ex) {
                Logger.getLogger(EchoThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (msg.getAction() == DEVICE_INFO) {
            List<MyPinState> list = new ArrayList<MyPinState>();
            for (Map.Entry<Region, Radiator_controler> entry : getRadiator_controler().entrySet()) {
                list.add(entry.getValue().getPinState());
            }
            for (Map.Entry<Region, Exaust_controler> entry : getExaust_controler().entrySet()) {
                list.add(entry.getValue().getPinState());
            }
            for (Map.Entry<Region, LightsControler> entry : getLights_controler().entrySet()) {
                list.add(entry.getValue().getPinState());
            }
            return new Message(gson.toJson(list));
        } else if (msg.getAction() == Config_info) {
            //automation information
            return new Message(gson.toJson(getAutomationControler().extract_controller_configuration()));
        }
        return null;
    }
    private final static Gson gson = new Gson();

    private Message findActionBasedonMessage(Message msg) throws InterruptedException, IOException {
        Message resp = check_for_info_req(msg);
        if (resp != null) {
            return resp;
        }
        switch (msg.getDevice()) {
            case Radiator: {
                System.out.println("Radiator....");
                if (msg.getAction() == START) {
                    getRadiator_controler().get(msg.getRegion()).start_radiator();
                    return new Message(getRadiator_controler().get(msg.getRegion()).toString(), true);
                } else if (msg.getAction() == STOP) {
                    getRadiator_controler().get(msg.getRegion()).stop_radiator();
                    return new Message(getRadiator_controler().get(msg.getRegion()).toString(), false);
                } else if (msg.getAction() == PERIOD_ACTIVATE) {
                    getRadiator_controler().get(msg.getRegion()).start_for_spacific_time_radiator(msg.getTtl());
                    return new Message(getRadiator_controler().get(msg.getRegion()).toString(), true);
                } else if (msg.getAction() == QUIT) {
                    getRadiator_controler().get(msg.getRegion()).deleteRadiator();
                    return new Message(getRadiator_controler().get(msg.getRegion()).toString(), false);
                }
                break;
            }
            case Exaust: {
                System.out.println(getExaust_controler().get(msg.getRegion()));
                System.out.println("Exaust...");
                if (msg.getAction() == START) {
                    getExaust_controler().get(msg.getRegion()).start_exaust();

                    return new Message(getExaust_controler().get(msg.getRegion()).toString(), true);
                } else if (msg.getAction() == STOP) {
                    getExaust_controler().get(msg.getRegion()).stop_exaust();
                    return new Message(getExaust_controler().get(msg.getRegion()).toString(), false);
                } else if (msg.getAction() == PERIOD_ACTIVATE) {
                    getExaust_controler().get(msg.getRegion()).stop_exaust();
                    return new Message(getExaust_controler().get(msg.getRegion()).toString(), true);
                } else if (msg.getAction() == QUIT) {
                    getExaust_controler().get(msg.getRegion()).start_for_spacific_time_exaust(msg.getTtl());
                    return new Message(getExaust_controler().get(msg.getRegion()).toString(), false);
                }
                break;
            }
            case Lights: {
                System.out.println(getLights_controler().get(msg.getRegion()));
                System.out.println("Lights...");
                if (msg.getAction() == START) {
                    getLights_controler().get(msg.getRegion()).start_lights();
                    return new Message(getLights_controler().get(msg.getRegion()).toString(), true);
                } else if (msg.getAction() == STOP) {
                    getLights_controler().get(msg.getRegion()).stop_lights();
                    return new Message(getLights_controler().get(msg.getRegion()).toString(), false);
                } else if (msg.getAction() == PERIOD_ACTIVATE) {
                    getLights_controler().get(msg.getRegion()).start_for_spacific_time_lights(msg.getTtl());
                    return new Message(getLights_controler().get(msg.getRegion()).toString(), true);
                } else if (msg.getAction() == QUIT) {
                    getLights_controler().get(msg.getRegion()).deleteLights();
                    return new Message(getLights_controler().get(msg.getRegion()).toString(), false);
                }
                break;
            }
            case Automation: {
                System.out.println("Automation...");
                if (msg.getAction() == START) {
                    System.out.println("Starting conntroler...");
                    startAutomation();
                    return new Message(gson.toJson(getAutomationControler().extract_controller_configuration()));
                } else if (msg.getAction() == STOP) {
                    System.out.println("Stoping Automation Controler...");
                    stoptAutomation();
                    return new Message(gson.toJson(getAutomationControler().extract_controller_configuration()));
                } else if (msg.getAction() == Config) {
                    System.out.println("Configureing automation");
                    Auto_Controler_config conf = msg.getConfig();
                    System.out.println(conf.getOpen_lights_time());
                    getAutomationControler().configControler(conf);
                    Thread.sleep(1000);
                    return new Message(gson.toJson(getAutomationControler().extract_controller_configuration()));
                }
                break;
            }
            default: {
                System.out.println("Cant see your point");
                break;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "EchoThread{" + "socket=" + socket + '}';
    }

}
