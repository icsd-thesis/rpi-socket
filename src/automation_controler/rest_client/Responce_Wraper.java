/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automation_controler.rest_client;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author onelove
 */
public class Responce_Wraper {

    private ArrayList<Sensor_Template> record_list;
    private Map<String, List<Sensor_Template>> map;
    private List<String> dateList;

    public void setMap(Map<String, List<Sensor_Template>> map) {
        this.map = map;
    }

    public void setDateList(ArrayList<String> dateList) {
        this.dateList = dateList;
    }

    public void setDateList(List<String> dateList) {
        this.dateList = dateList;
    }

    public Responce_Wraper() {
    }

    public Map<String, List<Sensor_Template>> getMap() {
        return map;
    }


    public List<String> getDateList() {
        return dateList;
    }


    public void setRecord_list(ArrayList<Sensor_Template> record_list) {
        this.record_list = record_list;
    }

    public ArrayList<Sensor_Template> getRecord_list() {
        return record_list;
    }

}
