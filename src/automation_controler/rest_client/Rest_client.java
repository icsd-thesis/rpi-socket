/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automation_controler.rest_client;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Rest_client {

    private static HttpURLConnection conn;
    private final static Gson json = new Gson();

    private static void constractRequest(URL url) throws ProtocolException, MalformedURLException, IOException {
        System.out.println("constractRequest");
        conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");
    }

    public static Responce_Wraper requestRest_api(URL url) {
        System.out.println("requestRest_api");
        String results = null;
        try {
            constractRequest(url);
            if (conn.getResponseCode() != 200) {
                System.out.println("Failed with code :" + conn.getResponseCode());
                System.out.println("Failed with message :" + conn.getResponseMessage());
                return null;
            }
            results = deserializeResults(conn.getInputStream());
            conn.disconnect();
            System.out.println(results);
            return parseJson(results);
        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        return null;
    }

    private final static String deserializeResults(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                (stream)));
        StringBuilder sb = new StringBuilder();
        try {

            String output;
            while ((output = br.readLine()) != null) {
                sb.append(output + "\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(Rest_client.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            br.close();
            return sb.toString();
        }

    }

    public static Responce_Wraper parseJson(String json) {
        return new Gson().fromJson(json, Responce_Wraper.class);
    }

}
